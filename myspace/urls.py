from django.urls import path
from . import views

app_name = 'myspace'

urlpatterns = [
    path('myspace/', views.myspace, name='space'),
    path('addSubject/', views.addSubject, name='addSubject'),
    path('listSubject/', views.listSubject, name='listSubject'),
    path('delete/P<int:delete_id>/', views.delete, name='delete'),
    # path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi')
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]