from . import forms
from .models import Subject
from django.shortcuts import render, redirect

# Create your views here.
def myspace(request):
    return render(request, 'space.html')

def addSubject(request):
    subject_form = forms.SubjectForm()
    if request.method == 'POST':
        subject_form_input = forms.SubjectForm(request.POST)
        if subject_form_input.is_valid():
            data = subject_form_input.cleaned_data
            subject_input = Subject()
            subject_input.subject_name = data['subject_name']
            subject_input.credits_amount = data['credits_amount']
            subject_input.room = data['room']
            subject_input.lecturer = data['lecturer']
            subject_input.year = data['year']
            subject_input.description = data['description']
            subject_input.save()
            current_data = Subject.objects.all()

            return redirect('/listSubject')  
        else:
            current_data = Subject.objects.all()
            return render(request, 'add.html',{'form':subject_form, 'status':'failed','data':current_data})
    else:
        current_data = Subject.objects.all()
        return render(request, 'add.html',{'form':subject_form,'data':current_data})


def listSubject(request):
    data = Subject.objects.all()
    return render(request, 'list.html',{'listSubject' : data})

def delete(request, delete_id):
    try:
        deleted_subject = Subject.objects.get(pk = delete_id)
        deleted_subject.delete()
        return redirect('/listSubject')
    except:
        return redirect('/listSubject')

