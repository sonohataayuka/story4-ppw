from django.urls import path
from . import views

app_name = 'HOMEPAGE'

urlpatterns = [
    path('', views.index, name='index'),
    path('gallery/', views.gallery, name='gallery'),
    path('gallery2/', views.gallery2, name='gallery2'),
    path('base/', views.base, name='base'),
    # path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi')
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]